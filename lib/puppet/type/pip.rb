Puppet::Type.newtype(:pip) do

  @doc = <<-EOS
  This type provides Puppet with the capabilities to manage Python pip packages.

  pip { 'some titla':
    ensure         => present,
    pkgname        => 'foobar',
    python_version => '2.7.5'
  }

  Or for a virtualenv

  pip { 'some titla':
    ensure         => present,
    pkgname        => 'foobar',
    virtualenv     => '/path/to/env',
    python_version => '2.7.5'
  }
  EOS

  ensurable do
    defaultvalues
    defaultto :present
  end

  newparam(:name, :namevar => true) do
    desc 'The pip package to be installed'
  end

  newparam(:pkgname) do
    desc 'The name of the package to install'
  end

  newparam(:user) do
    desc 'The owner for pip'
    defaultto 'root'
  end

  newparam(:pyenv_root) do
    desc 'The root of pyenv'
  end

  newparam(:group) do
    desc 'The group owner for pip'
    defaultto 'root'
  end

  newparam(:runas) do
    desc 'Run pip as a user.'
    defaultto 'root'
  end

  newparam(:install_dir) do
    desc 'Where the package should be installed'
  end

  newparam(:python_version) do
    desc 'Which python version should be used'
  end

  newparam(:virtualenv) do
    desc 'Which virtual environment should be used'
  end

  newparam(:requirements) do
    desc 'Install the requirements as well?'
  end
end


