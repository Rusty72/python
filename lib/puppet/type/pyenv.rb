Puppet::Type.newtype(:pyenv) do

  @doc = <<-EOS
    This type provides Puppet with the capabilities to manage Pythons by
    leveraging pyenv. Pyenv automatically installs pip for you.

    pyenv { '3.4.0':
      ensure      => present,
      runas       => 'someuser',
      install_dir => '/path/to/pyenv'
    }
  EOS

  ensurable do
    defaultvalues
    defaultto :present
  end

  newparam(:name, :namevar => true) do
    desc 'The python version that should be installed for pyenv'
    newvalues(/[\w.-]{3,}/)
  end

  newparam(:keep) do
    desc 'Keep source tree in $PYENV_ROOT/sources after installation'
    newvalues(true, false)
    defaultto false
  end

  newparam(:runas) do
    desc 'Run pyenv as a user.'
    defaultto 'root'
  end

  newparam(:install_dir) do
    desc 'Where pyenv is installed'
  end
end
