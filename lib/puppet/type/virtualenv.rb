Puppet::Type.newtype(:virtualenv) do

  @doc = <<-EOS
    This type provides Puppet with the capabilities to manage Python's virtual environments

    example:

    virtualenv{ $virtualenv:
      ensure         => present,
      python_version => '2.7.5',
      user           => 'someuser',
      group          => 'someuser',
      runas          => 'someuser',
      pyenv_root     => '/path/to/pyenv'
    }

  EOS

  ensurable do
    defaultvalues
    defaultto :present
  end

  newparam(:name, :namevar => true) do
    desc 'The name of the virtual env'
  end

  newparam(:user) do
    desc 'The owner of the virtual env'
    defaultto 'root'
  end

  newparam(:group) do
    desc 'The group owner of the virtual env'
    defaultto 'root'
  end

  newparam(:runas) do
    desc 'Run virtualenv as a user'
    defaultto 'root'
  end

  newparam(:python_version) do
    desc 'Which python version to specify for that virtual env'
  end

  newparam(:pyenv_root) do
    desc 'Where pyenv is installed'
  end

  newparam(:virtualenv) do
    desc 'The virtualenv to create'
  end
end

