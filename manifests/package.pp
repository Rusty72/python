# == Class: python::package
# Author: Siebren Zwerver
#

class python::package (
  $pyenv_ensure,
  $pyenv_version,
  $url,
  $user,
  $group,
  $install_dir,
  $dependencies,
){

  if $dependencies {
    $dependencies.each |String $package| {
      package { $package:
        ensure => installed,
        before => Vcsrepo['pyenv']
      }
    }
  }

  vcsrepo { 'pyenv':
    ensure   => $pyenv_ensure,
    path     => $install_dir,
    provider => git,
    source   => $url,
    revision => $pyenv_version,
    user     => $user
  }
}
