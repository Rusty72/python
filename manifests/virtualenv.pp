# == Define: python::virtualenv
#
define python::virtualenv (
  String $python_version,
  String $user          = $python::user,
  String $group         = $python::group,
  String $runas         = $user,
  String $virtualenv    = $title,
  String $pyenv_root    = $python::pyenv_install_dir,
  Enum[present, absent]  $ensure        = present,
) {
  virtualenv{ $virtualenv:
    ensure         => $ensure,
    python_version => $python_version,
    user           => $user,
    group          => $group,
    runas          => $user,
    pyenv_root     => $pyenv_root
  }
}
