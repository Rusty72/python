require 'spec_helper'
describe 'python' do
  context 'python::package' do
    let(:facts) do
      {
        'osfamily'                  => 'RedHat',
        'operatingsystem'           => 'RedHat',
        'path'                      => '/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin:/opt/puppetlabs/bin',
        'operatingsystemmajrelease' => '7',
      }
    end
    it { should contain_package('zlib-devel').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('readline-devel').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('bzip2-libs').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('readline').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('openssl-libs').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('openssl').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('patch').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('zlib').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('openssl-devel').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_package('bzip2-devel').with_ensure('installed').that_comes_before('Vcsrepo[pyenv]') }
    it { should contain_vcsrepo('pyenv') }
  end
end

