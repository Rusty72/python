require 'spec_helper'
describe 'python::pip' do
  context 'python::pip' do
    let(:title ) { 'Some Title' }
    let(:params) do
      {
        'python_version' => '2.7.5',
        'pkgname'        => 'test',
        'user'           => 'foo',
        'group'          => 'bar',
        'runas'          => 'foo',
        'pyenv_root'     => '/tmp/pyenv'
      }
    end
    let(:facts) do
      {
        'osfamily'                  => 'RedHat',
        'operatingsystem'           => 'RedHat',
        'path'                      => '/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin:/opt/puppetlabs/bin',
        'operatingsystemmajrelease' => '7',
      }
    end
    it { should contain_pip('Some Title').with(
      'pkgname'        => 'test',
      'user'           => 'foo',
      'group'          => 'bar',
      'runas'          => 'foo',
      'python_version' => '2.7.5',
      'pyenv_root'     => '/tmp/pyenv'
    )}
  end
end

