require 'spec_helper'
describe 'python::pyenv' do
  context 'python::pyenv' do
    let(:title ) { '1.2.5' }
    let(:params) do
      {
        'ensure'      => 'present',
        'version'     => '1.2.5',
        'runas'       => 'foo',
        'install_dir' => '/tmp/pyenv',
        'virtualenv'  => false
      }
    end
    let(:facts) do
      {
        'osfamily'                  => 'RedHat',
        'operatingsystem'           => 'RedHat',
        'path'                      => '/usr/local/bin:/bin:/usr/bin:/usr/X11R6/bin:/opt/puppetlabs/bin',
        'operatingsystemmajrelease' => '7',
      }
    end
    it { should contain_pyenv('1.2.5').with(
      'ensure'      => 'present',
      'runas'       => 'foo',
      'install_dir' => '/tmp/pyenv'
    )}
  end
end

